# Renata Arcos - Josefa Pinto San Martin / 29.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_agregar():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("ventana_agregar")
        self.ventana.set_title("AGREGAR PENDIENTE")
        self.ventana.maximize()
        self.ventana.show_all()
        
        # boton para eliminar un pendiente en especifico
        add = builder.get_object("agregar_nuevo")
        add.connect("clicked", self.agregar_nuevo)
        add.set_label("AGREGAR")

        # entrada para nuevo pendiente
        self.entry = builder.get_object("entrada_nombre_nuevo")
        label_pendiente = builder.get_object("label_nombre_nuevo")
        label_pendiente.set_label("INGRESE EL NUEVO PENDIENTE:")
                
        # label para el calendario
        label_calendario = builder.get_object("label_calendario_agregar")
        label_calendario.set_label("INGRESE UNA FECHA PARA EL PENDIENTE")

        #calendario
        self.calendario = builder.get_object("calendario_agregar")

    def agregar_nuevo(self, btn=None):
        original = []
        archivo = open("pendientes.csv")
                
        # obtener pendiente
        self.new_pendiente = self.entry.get_text()
        self.new_pendiente = self.new_pendiente.upper()

        # obtener y formatear fecha
        fecha = self.calendario.get_date()
        dia = fecha[2]
        mes = fecha[1]+1
        año = fecha[0]
        self.new_date = f"{dia}-{mes}-{año}"
        
        # hacer lista con los pendientes
        linea = 1 # inicializador
        while linea != "":
                linea = archivo.readline()
                original.append(linea)

        # formatear lista
        original.remove("")
        nueva_linea = f"\n{self.new_pendiente}, {self.new_date}"
        if self.new_pendiente == "":
            pass
        else:
            original.append(nueva_linea)

        # reescribir el archivo a partir de la lista creada
        with open("pendientes.csv", "w") as file:
            file.write("".join(original))
        file.close()
        self.ventana.destroy()
        mensaje_agregar(self.new_pendiente)


class mensaje_agregar():
    def __init__(self, pendiente):
        """Entrega un mensaje al usaurio luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("mensaje_agregar")
        self.ventana.set_markup(f'PENDIENTE "{pendiente}"')
        self.ventana.format_secondary_markup(f'AGREGADO CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar
        close = builder.get_object("cerrar2")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None):
        self.ventana.destroy()

