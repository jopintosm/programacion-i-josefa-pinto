# Josefa Pinto San Martin / 07.12.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from ver import ventana_ver
from ingresar import ventana_ingresar
from eliminar import ventana_eliminar
from resumen import ventana_resumen

class ventana_principal():
    
    def __init__(self):
        """Generar y mostrar ventana maximizada, además
        se termina el programa al cerrar la ventana."""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("principal")
        self.ventana.set_title("VENTANA PRINCIPAL")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.show_all()

        # mensaje principal
        label_principal = builder.get_object("label_principal")
        label_principal.set_label("POR FAVOR ELIJA UNA OPCION PARA CONTINUAR")

        # boton para mostrar todos los datos almacenados
        ver = builder.get_object("ver")
        ver.connect("clicked", self.ver_datos)
        ver.set_label("VER DATOS")

        # boton para mostrar un resumen de los datos
        resumen = builder.get_object("resumen")
        resumen.connect("clicked", self.resumen_datos)
        resumen.set_label("RESUMEN DE DATOS")

        # boton para eliminar una linea de datos en especifico
        eliminar = builder.get_object("eliminar")
        eliminar.connect("clicked", self.eliminar_dato)
        eliminar.set_label("ELIMINAR DATO")
        
        # boton para ingresar datos
        ingresar = builder.get_object("ingresar")
        ingresar.connect("clicked", self.ingresar_dato)
        ingresar.set_label("INGRESAR DATO")


    def ver_datos(self, click=None):
        ventana_ver()


    def resumen_datos(self, click=None):
        ventana_resumen()


    def eliminar_dato(self, click=None):
        ventana_eliminar()
    
    
    def ingresar_dato(self, click=None):
        ventana_ingresar()
    

if __name__ == "__main__":
    ventana_principal()
    Gtk.main()