# Josefa Pinto San Martin / 07.12.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_ingresar():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("ventana_ingresar")
        self.ventana.set_title("INGRESAR DATOS")
        self.ventana.maximize()
        self.ventana.show_all()
        
        # boton para confirmar el nuevo ingreso
        add = builder.get_object("ingresar_nuevo")
        add.connect("clicked", self.ingresar_nuevo)
        add.set_label("INGRESAR")

        # label ingresar
        label_ingresar = builder.get_object("label_ingresar")
        label_ingresar.set_label("POR FAVOR COMPLETE LOS DATOS SOLICITADOS")

        # entrada para sodio
        self.entry_sodio = builder.get_object("entrada_sodio")
        label_sodio = builder.get_object("label_sodio")
        label_sodio.set_label("INGRESE EL VALOR DE SODIO EN LA SANGRE:")

    # Crear combo box edad
    # para todos los combo box se realiza el mismo proceso
        # label para edad
        label_edad = builder.get_object("label_edad")
        label_edad.set_label("SELECCIONE LA EDAD:")

        # realizar combo box para edad
        self.combo_edad = builder.get_object("combo_edad")
        self.lista_edad = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo_edad.pack_start(cell, True)
        self.combo_edad.add_attribute(cell, "text", 0)
        self.combo_edad.connect("changed", self.elegir_edad)

        # crear lista para añadir al modelo
        edades = []
        for item in range(100):
            edades.append(item+1)
        edades = [str(x) for x in edades]
        # Se añaden pendientes a lista modelo
        for edad in edades:
                self.lista_edad.append([edad])

        # Se carga modelo en combo box
        self.combo_edad.set_model(self.lista_edad)

    # Crear combo box sexos   
        # label para sexos
        label_sex = builder.get_object("label_sexo")
        label_sex.set_label("INGRESE EL SEXO:")
        
        # realizar combo box para sexos
        self.combo_sexos = builder.get_object("combo_sexo")
        self.lista_sexos = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo_sexos.pack_start(cell, True)
        self.combo_sexos.add_attribute(cell, "text", 0)
        self.combo_sexos.connect("changed", self.elegir_sexo)

        self.sexos = ["M", "F"]
        for sexo in self.sexos:
            self.lista_sexos.append([sexo])
        self.combo_sexos.set_model(self.lista_sexos)

    # Crear combo box bp
        # label para bp
        label_bp = builder.get_object("label_bp")
        label_bp.set_label("SELECCIONE EL NIVEL DE PRESION SANGUINEA:")

        # realizar combo box para bp
        self.combo_bp = builder.get_object("combo_bp")
        self.lista_bp = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo_bp.pack_start(cell, True)
        self.combo_bp.add_attribute(cell, "text", 0)
        self.combo_bp.connect("changed", self.elegir_bp)

        presiones = [ "LOW", "NORMAL", "HIGH"]
        for bp in presiones:
                self.lista_bp.append([bp])
        self.combo_bp.set_model(self.lista_bp)

    # Crear combo box droga
        # label para droga
        label_droga = builder.get_object("label_droga")
        label_droga.set_label("SELECCIONE LA DROGA:")

        # realizar combo box para droga
        self.combo_droga = builder.get_object("combo_droga")
        self.lista_droga = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo_droga.pack_start(cell, True)
        self.combo_droga.add_attribute(cell, "text", 0)
        self.combo_droga.connect("changed", self.elegir_droga)

        drogas = ["drugA", "drugB", "drugC", "drugX", "drugY"]
        for droga in drogas:
            self.lista_droga.append([droga])
        self.combo_droga.set_model(self.lista_droga)

    # Crear combo box colesterol
        # label para colesterol
        label_colesterol = builder.get_object("label_colesterol")
        label_colesterol.set_label("SELECCIONE EL NIVEL DE COLESTEROL:")

        # realizar combo box para colesterol
        self.combo_colesterol = builder.get_object("combo_colesterol")
        self.lista_colesterol = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo_colesterol.pack_start(cell, True)
        self.combo_colesterol.add_attribute(cell, "text", 0)
        self.combo_colesterol.connect("changed", self.elegir_colesterol)

        colesteroles = ["NORMAL", "HIGH"]
        for colesterol in colesteroles:
                self.lista_colesterol.append([colesterol])
        self.combo_colesterol.set_model(self.lista_colesterol)

    # se realiza el mismo proceso para seleccionar un
    # elemento en cada combo box            
    def elegir_edad(self, btn=None):
        # Se obtiene iterador y modelo
        iterator = self.combo_edad.get_active_iter()
        modelo = self.combo_edad.get_model()
        # De lo anterior se obtiene el elemento
        self.choise_edad = modelo[iterator][0]

    def elegir_sexo(self, btn=None):
        iterator = self.combo_sexos.get_active_iter()
        modelo = self.combo_sexos.get_model()
        self.choise_sexo = modelo[iterator][0]

    def elegir_bp(self, btn=None):
        iterator = self.combo_bp.get_active_iter()
        modelo = self.combo_bp.get_model()
        self.choise_bp = modelo[iterator][0]
    
    def elegir_droga(self, btn=None):
        iterator = self.combo_droga.get_active_iter()
        modelo = self.combo_droga.get_model()
        self.choise_droga = modelo[iterator][0]
    
    def elegir_colesterol(self, btn=None):
        iterator = self.combo_colesterol.get_active_iter()
        modelo = self.combo_colesterol.get_model()
        self.choise_colesterol = modelo[iterator][0]


    def ingresar_nuevo(self, btn=None):
        try:
            
            edad = self.choise_edad
            sexo = self.choise_sexo
            bp = self.choise_bp
            colesterol = self.choise_colesterol
            droga = self.choise_droga
            sodio = self.entry_sodio.get_text()
            
            # hacer que numeros decimales sean validados
            try:
                sodio = float(sodio)
            except ValueError:    
                self.entry_sodio.set_text("")
                sodio = "error"
                # ventana aviso de no es numero.
                no_numero()

            # hacer lista con cada linea
            original = []
            archivo = open("drug200.csv")
            linea = 1 # inicializador
            while linea != "":
                    linea = archivo.readline()
                    original.append(linea)

            # formatear lista
            if sodio == "error":
                pass
            else:
                nueva_linea = f"\n{edad}, {sexo}, {bp}, {colesterol}, {sodio}, {droga}"
                original.append(nueva_linea)
            original.remove("")
            
            # reescribir el archivo a partir de la lista creada
            with open("drug200.csv", "w") as file:
                file.write("".join(original))
            file.close()
            

            if sodio != "error":
                # ventana de aviso que se ingresó correctamente.
                listo_ingresar()
                self.ventana.destroy()

        except AttributeError:
            # mensaje que avisa que no relleno todos los campos
            incompleto()

class listo_ingresar():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("mensaje_listo_ingresar")
        self.ventana.set_markup('NUEVOS DATOS')
        self.ventana.format_secondary_markup('INGRESADOS CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

class incompleto():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("mensaje_incompleto")
        self.ventana.set_markup('NO RELLENÓ TODOS LOS CAMPOS,')
        self.ventana.format_secondary_markup('POR FAVOR INTENTELO NUEVAMENTE')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar1")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

class no_numero():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("mensaje_numero")
        self.ventana.set_markup('EL VALOR PARA EL SODIO NO ES UN NUMERO,')
        self.ventana.format_secondary_markup('POR FAVOR INGRESE UN NUMERO')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar4")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()
