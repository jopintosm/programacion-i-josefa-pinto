# Renata Arcos - Josefa Pinto San Martin / 17.12.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_about():
    
    def __init__(self):
    
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("ventana_about")
        self.ventana.set_title("ABOUT US")
        self.ventana.set_default_size(600, 300)
        self.ventana.show_all()

        # setear los diferentes textos del about us
        self.ventana.set_authors(["RENATA ARCOS", "JOSEFA PINTO"])
        self.ventana.set_program_name("GESTOR DE COMENTARIOS POR DROGA")
        self.ventana.set_version("Versión 1.0")
        self.ventana.set_comments("Profesor Fabio Durán Verdugo")
        self.ventana.set_copyright("Ingeniería Civil en Bioinformática - Programacion I")
