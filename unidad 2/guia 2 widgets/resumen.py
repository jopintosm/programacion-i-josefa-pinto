# Josefa Pinto San Martin / 07.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_resumen():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("ventana_resumen")
        self.ventana.set_title("RESUMEN DE DATOS")
        self.ventana.show_all()

        # reconocer labels y setear algunos.
        label_edades = builder.get_object("label_promedio_edades")
        label_mujeres = builder.get_object("label_mujeres")
        label_hombres = builder.get_object("label_hombres")
        label_bp = builder.get_object("label_bp1")
        label_bp.set_label("CANTIDAD DE PERSONAS EN CADA NIVEL DE PRESION SANGUINEA")
        cantidad_bp = builder.get_object("cantidad_bp")
        label_colesterol = builder.get_object("label_colesterol1")
        label_colesterol.set_label("CANTIDADE DE PERSONAS EN CADA NIVEL DE COLESTEROL")
        cantidad_colesterol = builder.get_object("cantidad_colesterol")
        label_sodio = builder.get_object("label_sodio1")
        label_droga = builder.get_object("label_droga1")
        label_droga.set_label("CANTIDAD DE CADA DROGA USADA")
        cantidad_drogas = builder.get_object("cantidad_drogas")

        # Hacer listas con los datos del archivo
        lista_edades = []
        lista_sexos = []
        lista_bp = []
        lista_colesterol = []
        lista_sodio = []
        lista_drogas = []
        archivo = open("drug200.csv")
        linea = 1 # inicializador
        contador = 0
        while linea != "":
            linea = archivo.readline()
            contador += 1
            if contador == 1:
                continue
            if linea == "":
                continue
            aux = linea.strip().split(",")
            edad = aux[0]
            lista_edades.append(edad)
            sexo = aux[1]
            lista_sexos.append(sexo)
            bp = aux[2]
            lista_bp.append(bp)
            colesterol = aux[3]
            lista_colesterol.append(colesterol)
            sodio = aux[4]
            lista_sodio.append(sodio)
            droga = aux[5]
            lista_drogas.append(droga)

        # formatear lista de edades y calcular promedio.
        lista_edades = [int(x) for x in lista_edades]
        contador = 0
        for item in range(len(lista_edades)):
            contador =  contador + lista_edades[item]
        promedio_edad = contador / len(lista_edades)
        
        # contar cantidad de cada sexo
        mujeres = lista_sexos.count("F")
        hombres = lista_sexos.count("M")

        # contar cantidad de cada nivel de bp
        low_bp = lista_bp.count("LOW")
        normal_bp = lista_bp.count("NORMAL")
        high_bp = lista_bp.count("HIGH")

        # contar cantidad de cada nivel de colesterol
        normal = lista_colesterol.count("NORMAL")
        high = lista_colesterol.count("HIGH")

        # formatear lista de sodio y calcular promedio.
        lista_sodio = [float(x) for x in lista_sodio]
        contador1 = 0
        for item1 in range(len(lista_sodio)):
            contador1 =  contador1 + lista_sodio[item1]
        promedio_sodio = round((contador1 / len(lista_sodio)), 3)

        # contar cantidad de cada droga utilizada
        A = lista_drogas.count("drugA")
        B = lista_drogas.count("drugB")
        C = lista_drogas.count("drugC")
        X = lista_drogas.count("drugX")
        Y = lista_drogas.count("drugY")

        # formatear labels
        label_edades.set_label(f"LA EDAD PROMEDIO ES: {promedio_edad} AÑOS")
        label_mujeres.set_label(f"HAY {mujeres} MUJERES REGISTRADAS")
        label_hombres.set_label(f"HAY {hombres} HOMBRES REGISTRADOS")
        cantidad_bp.set_label(f"NIVEL LOW : {low_bp}  |  NIVEL NORMAL : {normal_bp}  |  NIVEL HIGH : {high_bp}")
        cantidad_colesterol.set_label(f"NIVEL NORMAL : {normal}  |  NIVEL HIGH : {high}")
        label_sodio.set_label(f"EL VALOR PROMEDIO PARA EL SODIO ES: {promedio_sodio}")
        cantidad_drogas.set_label(f"Drug A : {A}  |  Drug B : {B}  |  Drug C : {C}  |  Drug X : {X}  |  Drug Y : {Y}")
        