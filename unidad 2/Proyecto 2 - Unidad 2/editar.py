# Renata Arcos - Josefa Pinto San Martin / 17.12.2021
import pandas as pd
import numpy as np
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogos import incompleto, no_numero, editado

class ventana_editar():
    
    def __init__(self,review,ID,datos,path,nombre):
    
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("ventana_editar")
        self.ventana.set_title("EDITAR DATO")
        self.ventana.set_default_size(800,400)
        self.ventana.show_all()
        self.review = review
        self.ID = ID
        self.datos = datos
        self.path = path
        self.nombre = nombre
        
        # boton aceptar
        self.aceptar_btn = builder.get_object("boton_aceptar")
        self.aceptar_btn.connect("clicked", self.ingresar)
        self.aceptar_btn.set_label("ACEPTAR")

        # boton cancelar
        cancelar_btn = builder.get_object("boton_cancelar")
        cancelar_btn.connect("clicked", self.cancelar)
        cancelar_btn.set_label("CANCELAR")
    
        # label ID
        self.label_ID = builder.get_object("label_ID")
        self.label_ID.set_label(f"ID: {self.ID}")

        # label fecha
        self.label_fecha = builder.get_object("label_fecha")
        self.label_fecha.set_label("Fecha")

        # label nombre
        self.label_nombre = builder.get_object("label_nombre")
        self.label_nombre.set_label("Nombre de la droga ")

        # label condicion
        self.label_condicion = builder.get_object("label_condicion")
        self.label_condicion.set_label("Condicion")

        # label rating
        self.label_rating = builder.get_object("label_rating")
        self.label_rating.set_label("Rating")

        # label review
        label_comentario = builder.get_object("label_review")
        label_comentario.set_label("Comentario")

        #label nombre
        label_nombre = builder.get_object("label_drugName")
        label_nombre.set_label(self.nombre)

        # entradas
        self.entry_condicion = builder.get_object("entrada_condicion")
        self.entry_rating = builder.get_object("entrada_rating")

        # calendario
        self.calendario = builder.get_object("calendario")

        # se carga el text view y su buffer(el texto que muestra)
        self.textview = builder.get_object("textview")
        self.text = self.textview.get_buffer()

        # se setea el texto del textview (comentario actual)
        self.review = self.datos.loc[self.datos["uniqueID"]==self.ID,"review"].iloc[0]
        self.text.set_text(self.review)


    def ingresar(self, btn=None):
        #lista para pasar el mes de numero a nombre
        meses = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                 "Aug", "Sep", "Oct", "Nov", "Dec"]

        # obtener y formatear fecha
        fecha = self.calendario.get_date()
        dia = fecha[2]
        mes = fecha[1]+1
        mes = meses[mes-1]
        año = str(fecha[0])
        año = list(año)
        año = f"{año[-2]}{año[-1]}"

        # se obtienen los valores para cada variable
        condicion = self.entry_condicion.get_text()
        rating = self.entry_rating.get_text()

        # obtener comentario del textview
        start = self.text.get_start_iter()
        end = self.text.get_end_iter()
        comentario = self.text.get_text(start, end, False)

        # discrimina si es un numero entero entre 1 y 10 o no
        try:
            if int(rating) < 1 or int(rating) > 10 or rating.isnumeric() == False:
                no_numero()
                self.entry_rating.set_text("")
        except ValueError:
            no_numero()
            self.entry_rating.set_text("")
        
        # formatear datos
        condicion = condicion.strip().title()
        rating = rating.strip()
        date = f"{dia}-{mes}-{año}"

        if self.nombre != "" and condicion != "" and comentario !=  "" and rating != "":
            # el numero 0 corresponde a "usefulCount", ya que se reiniciaría.
            #line = f"{self.ID},{self.nombre},{condicion},{comentario},{rating},{date},0"
            nueva_fila = {"uniqueID": self.ID, "drugName": self.nombre,"condition": condicion,"review": comentario, "rating": rating,"date" : date, "usefulCount": "0"}
        else:
            # mensaje que avisa al usuario que está incompleto
            incompleto()

        self.datos = self.datos.append(nueva_fila, ignore_index=True)
        self.datos.to_csv(self.path)
        editado()
        self.ventana.destroy()

    def cancelar(self, btn=None):
        self.ventana.destroy()
