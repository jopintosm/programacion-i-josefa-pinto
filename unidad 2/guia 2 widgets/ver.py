# Josefa Pinto San Martin / 07.12.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_ver():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("ventana_ver")
        self.ventana.set_title("VER DATOS")
        self.ventana.set_default_size(425, 600)
        self.ventana.show_all()
        
        # generar tree y modelo
        tree = builder.get_object("tree")
        
        # establecer modelo
        self.modelo = Gtk.ListStore(str, str, str, str, str, str)
        tree.set_model(model=self.modelo)

        # generar columnas
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("AGE",
                                cell,
                                text=0)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("SEX",
                                cell,
                                text=1)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("BP",
                                cell,
                                text=2)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CHOLESTEROL",
                                cell,
                                text=3)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Na_to_K",
                                cell,
                                text=4)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("DRUG",
                                cell,
                                text=5)
        tree.append_column(column)

        # crear "tabla" con datos
        archivo = open("drug200.csv")
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            if linea == "":
                continue
            substring = linea.strip().split(",")
            age = substring[0]
            sex = substring[1]
            bp = substring[2]
            colesterol = substring[3]
            sodio = substring[4]
            droga = substring[5]
            self.modelo.append([age, sex, bp, colesterol, sodio, droga])