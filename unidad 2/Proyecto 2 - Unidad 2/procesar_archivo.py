import pandas as pd

# leer el archivo original
datos = pd.read_csv("drugsComTest_raw.csv")

# confirmacion para dividir los archivos
opcion = input("¿Desea dividir los archivos por año? s/n\n")

if opcion == "s" or opcion == "S":
    # filtrar datos por año y hacer un nuevo archivo
    # con los datos ya filtrados
    l08 = datos.loc[datos['date'].str.contains('-08')]
    l08.to_csv('2008.csv')

    l09 = datos.loc[datos['date'].str.contains('-09')]
    l09.to_csv('2009.csv')

    l10 = datos.loc[datos['date'].str.contains('-10')]
    l10.to_csv('2010.csv')

    l11 = datos.loc[datos['date'].str.contains('-11')]
    l11.to_csv('2011.csv')

    l12 = datos.loc[datos['date'].str.contains('-12')]
    l12.to_csv('2012.csv')

    l13 = datos.loc[datos['date'].str.contains('-13')]
    l13.to_csv('2013.csv')

    l14 = datos.loc[datos['date'].str.contains('-14')]
    l14.to_csv('2014.csv')

    l15 = datos.loc[datos['date'].str.contains('-15')]
    l15.to_csv('2015.csv')

    l16 = datos.loc[datos['date'].str.contains('-16')]
    l16.to_csv('2016.csv')

    l17 = datos.loc[datos['date'].str.contains('-17')]
    l17.to_csv('2017.csv')
    print("TODOS LOS ARCHIVOS FUERON CREADOS EXITOSAMENTE")
else:
    print("Ha seleccionado no dividir el archivo")
