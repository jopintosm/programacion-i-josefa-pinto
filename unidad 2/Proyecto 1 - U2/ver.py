# Renata Arcos - Josefa Pinto San Martin / 29.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_ver():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("ventana_ver")
        self.ventana.set_title("VER PENDIENTES")
        self.ventana.set_default_size(350, 450)
        self.ventana.show_all()
        
        # generar tree y modelo
        tree = builder.get_object("tree")
        
        # establecer modelo
        self.modelo = Gtk.ListStore(str, str)
        tree.set_model(model=self.modelo)

        # generar columnas
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PENDIENTE",
                                cell,
                                text=0)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("FECHA",
                                cell,
                                text=1)
        tree.append_column(column)

        # crear "tabla" con pendientes y sus fechas
        # obtenidas de un archivo csv
        archivo = open("pendientes.csv")
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            substring = linea.strip().split(",")
            pendiente = substring[0]
            fecha = substring[-1]
            if pendiente == "":
                pass
            else:
            # añadir al modelo cada linea del archivo
                self.modelo.append([pendiente,fecha])