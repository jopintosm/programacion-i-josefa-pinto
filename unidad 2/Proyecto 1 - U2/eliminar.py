# Renata Arcos - Josefa Pinto San Martin / 29.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_eliminar():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("ventana_eliminar")
        self.ventana.set_title("ELIMINAR PENDIENTE")
        self.ventana.maximize()
        self.ventana.show_all()

        # mensaje label
        label_combo = builder.get_object("label_combo_eliminar")
        label_combo.set_label("SELECCIONE UN PENDIENTE PARA ELIMINAR")

        # realizar combo box
        self.combo = builder.get_object("combo_eliminar")
        self.lista = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo.pack_start(cell, True)
        self.combo.add_attribute(cell, "text", 0)
        self.combo.connect("changed", self.elegir_presionado)

        # crear lista con pendientes
        self.pendientes = []
        archivo = open("pendientes.csv")
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            substring = linea.strip().split(",")
            pendiente = substring[0]
            self.pendientes.append(pendiente)
        self.pendientes.remove("")
        archivo.close()

        # Se añaden pendientes a lista modelo
        for pendiente in self.pendientes:
            self.lista.append([pendiente])

        # Se carga modelo en combo box
        self.combo.set_model(self.lista)
        
        # boton para confirmar
        eliminar = builder.get_object("confirmar_eliminar")
        eliminar.connect("clicked", self.eliminar_pendiente)
        eliminar.set_label("ELIMINAR")

    # Método para seleccionar un elemento
    def elegir_presionado(self, btn=None):
        # Se obtiene iterador y modelo
        iterator = self.combo.get_active_iter()
        modelo = self.combo.get_model()
        # De lo anterior se obtiene el elemento
        self.seleccion = modelo[iterator][0]
        
    def eliminar_pendiente(self, btn=None):    
        self.pendientes.remove(self.seleccion)

        # quitar de la lista original
        original = []
        archivo = open("pendientes.csv")
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            substring = linea.strip().split(",")
            pendiente = substring[0]
            if pendiente == self.seleccion:
                linea = ".\n"
            original.append(linea)
        original.remove(".\n")

        # reescribir el archivo a partir de la lista creada
        with open("pendientes.csv", "w") as file:
            file.write("".join(original))
        file.close()
        self.ventana.destroy()
        mensaje_eliminar(self.seleccion)


class mensaje_eliminar():
    def __init__(self, pendiente):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("mensaje_eliminar")
        self.ventana.set_markup(f'PENDIENTE "{pendiente}"')
        self.ventana.format_secondary_markup(f'ELIMINADO CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar ventana de dialogo
        close = builder.get_object("cerrar")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")


    def cerrar_mensaje(self, click=None):
        self.ventana.destroy()
    
    

if __name__ == "__main__":
    ventana_eliminar()
    Gtk.main()