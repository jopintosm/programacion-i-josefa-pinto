# Josefa Pinto San Martin / 07.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana_eliminar():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("ventana_eliminar")
        self.ventana.set_title("ELIMINAR DATO")
        self.ventana.maximize()
        self.ventana.show_all()

        # mensaje label
        label_combo = builder.get_object("label_combo_eliminar")
        label_combo.set_label("SELECCIONE UNA LINEA PARA ELIMINAR")

        # realizar combo box
        self.combo = builder.get_object("combo_eliminar")
        self.lista = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo.pack_start(cell, True)
        self.combo.add_attribute(cell, "text", 0)
        # como colocarle nombre a combobox
        self.combo.connect("changed", self.elegir_presionado)

        # crear lista con cada linea
        self.datos = []
        archivo = open("drug200.csv")
        line = 1 # inicializador
        # contador para que no se guarde la primera linea.
        contador = 0
        while line != "":
            line = archivo.readline()
            contador += 1
            if contador == 1:
                continue
            line = line.replace(",","  |  ")
            self.datos.append(line)
        self.datos.remove("")
        archivo.close()

        # Se añaden pendientes a lista modelo
        for dato in self.datos:
            self.lista.append([dato])

        # Se carga modelo en combo box
        self.combo.set_model(self.lista)
        
        # boton para confirmar
        eliminar = builder.get_object("confirmar_eliminar")
        eliminar.connect("clicked", self.eliminar_dato)
        eliminar.set_label("ELIMINAR")

    # Método para seleccionar un elemento
    def elegir_presionado(self, btn=None):
        # Se obtiene iterador y modelo
        iterator = self.combo.get_active_iter()
        modelo = self.combo.get_model()
        # De lo anterior se obtiene el elemento
        self.seleccion1 = modelo[iterator][0]
        self.seleccion = self.seleccion1.replace("  |  ",",")
        
    def eliminar_dato(self, btn=None):    
        try:
            self.datos.remove(self.seleccion1)
            # quitar de la lista original
            original = []
            archivo = open("drug200.csv")
            linea = 1 # inicializador
            while linea != "":
                linea = archivo.readline()
                if linea == self.seleccion:
                    continue
                original.append(linea)

            # reescribir el archivo a partir de la lista creada
            with open("drug200.csv", "w") as file:
                file.write("".join(original))
            file.close()
            self.ventana.destroy()
            mensaje_eliminar()
        except AttributeError:
            # ventana de aviso que no seleccionó el dato para eliminar.
            incompleto()


class mensaje_eliminar():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("mensaje_eliminar")
        self.ventana.set_markup(f'DATO')
        self.ventana.format_secondary_markup(f'ELIMINADO CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar ventana de dialogo
        close = builder.get_object("cerrar2")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None):
        self.ventana.destroy()

class incompleto():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("guia2.ui")
        self.ventana = builder.get_object("mensaje_incompleto1")
        self.ventana.set_markup('POR FAVOR SELECCIONE')
        self.ventana.format_secondary_markup('UN DATO PARA ELIMINAR')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar3")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")


    def cerrar_mensaje(self, click=None):
        self.ventana.destroy()
