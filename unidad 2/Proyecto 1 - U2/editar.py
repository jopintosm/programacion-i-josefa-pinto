# Renata Arcos - Josefa Pinto San Martin / 29.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class ventana_editar():
    
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("ventana_editar")
        self.ventana.set_title("EDITAR PENDIENTE")
        self.ventana.maximize()
        self.ventana.show_all()

        # mensaje label combo
        label_combo = builder.get_object("label_combo_editar")
        label_combo.set_label("SELECCIONE UN PENDIENTE PARA EDITAR")

        # realizar combo box
        self.combo = builder.get_object("combo_editar")
        self.lista = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo.pack_start(cell, True)
        self.combo.add_attribute(cell, "text", 0)
        self.combo.connect("changed", self.seleccionar_presionado)

        # crear lista con pendientes
        self.pendientes = []
        archivo = open("pendientes.csv")
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            substring = linea.strip().split(",")
            pendiente = substring[0]
            self.pendientes.append(pendiente)
        self.pendientes.remove("")
        archivo.close()

        # Se añaden pendientes a lista modelo
        for pendiente in self.pendientes:
            self.lista.append([pendiente])

        # Se carga modelo en combo box
        self.combo.set_model(self.lista)

        # entrada para nuevo pendiente
        self.entry = builder.get_object("entrada_new_pendiente")
        label_pendiente = builder.get_object("label_new_pendiente")
        label_pendiente.set_label("INGRESE EL NUEVO PENDIENTE:")

        # label para el calendario
        label_calendario = builder.get_object("label_calendario_editar")
        label_calendario.set_label("INGRESE UNA FECHA PARA EL PENDIENTE")

        # calendario
        self.calendario = builder.get_object("calendario_editar")

        # boton para confirmar
        edit = builder.get_object("confirmar_editar")
        edit.connect("clicked", self.editar_pendiente)
        edit.set_label("EDITAR")
        
    # Método para seleccionar un elemento
    def seleccionar_presionado(self, btn=None):
        # Se obtiene iterador y modelo
        iterator = self.combo.get_active_iter()
        modelo = self.combo.get_model()
        # De lo anterior se obtiene el elemento seleccionado.
        self.seleccion = modelo[iterator][0]

    def editar_pendiente(self, btn=None):
        self.pendientes.remove(self.seleccion)
        original = []
        archivo = open("pendientes.csv")

        # obtener pendiente        
        self.new_pendiente = self.entry.get_text()
        self.new_pendiente = self.new_pendiente.upper()
        
        # obtener y formatear fecha
        fecha = self.calendario.get_date()
        dia = fecha[2]
        mes = fecha[1]+1
        año = fecha[0]
        self.new_date = f"{dia}-{mes}-{año}"
        
        linea = 1 # inicializador
        while linea != "":
            linea = archivo.readline()
            substring = linea.strip().split(",")
            pendiente = substring[0]
            fecha = substring[-1]
            if pendiente == self.seleccion:
                aux = linea
                linea = f"{self.new_pendiente}, {self.new_date}\n"
                #evitar guardar valores nulos
                if self.new_pendiente == "":
                    linea = aux
            original.append(linea)

        # reescribir el archivo a partir de la lista creada
        with open("pendientes.csv", "w") as file:
            file.write("".join(original))
        file.close()
        
        # cerrar la ventana
        self.ventana.destroy()
        mensaje_editar(self.seleccion)
    

class mensaje_editar():
    def __init__(self, pendiente):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("mensaje_editar")
        self.ventana.set_markup(f'PENDIENTE "{pendiente}"')
        self.ventana.format_secondary_markup(f'EDITADO CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar1")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")


    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

