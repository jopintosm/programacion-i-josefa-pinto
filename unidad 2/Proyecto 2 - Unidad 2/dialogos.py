# Renata Arcos - Josefa Pinto San Martin / 17.12.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class incompleto():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("mensaje_incompleto")
        self.ventana.set_markup('NO RELLENÓ TODOS LOS CAMPOS,')
        self.ventana.format_secondary_markup('POR FAVOR INTENTELO NUEVAMENTE')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

class no_numero():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("mensaje_nonumero")
        self.ventana.set_markup('EL VALOR PARA EL RATING NO ES VÁLIDO,')
        self.ventana.format_secondary_markup('POR FAVOR INGRESE UN NUMERO ENTRE 1 Y 10')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar1")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

class select_fila():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("mensaje_fila")
        self.ventana.set_markup('PORFAVOR SELECCIONE UNA FILA ')
        self.ventana.format_secondary_markup('CON DOBLE CLICK')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar2")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()

class editado():
    def __init__(self):
        """Entrega un mensaje al usuario luego
        de realizar alguna accion"""
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("mensaje_editado")
        self.ventana.set_markup('EDITADO ')
        self.ventana.format_secondary_markup('CORRECTAMENTE')
        self.ventana.show_all()

        # boton para cerrar dialogo
        close = builder.get_object("cerrar3")
        close.connect("clicked", self.cerrar_mensaje)
        close.set_label("CERRAR")

    def cerrar_mensaje(self, click=None): 
        self.ventana.destroy()