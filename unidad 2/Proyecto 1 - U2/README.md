#Proyecto 1 - Unidad 2 / Renata Arcos - Josefa Pinto

El segundo semestre de nuestro primer año de universidad se ha
caracterizado por constantemente tener múltiples tareas, por lo 
que el tiempo ser debe utilizar de manera óptima y para lograr
esto, la organización es fundamental.
 
Nuestro programa está enfocado en todos aquellas personas que
poseen múltiples tareas por realizar, como ayuda para la
organización personal y para evitar que se olviden ciertos pendientes.

Al ejecutar el programa, se abre una ventana principal donde este ofrece
cuatro opciones, la primera "VER PENDIENTES", donde al presionar el boton
muestra el listado de las tareas pendientes que se tienen. La segunda
corresponde a "EDITAR PENDIENTE", donde un pendiente en especifico, se puede
editar el texto y su fecha. La tercera opción "ELIMINAR PENDIENTE", permite
eliminar un pendiente en especifico. Y la última opción, "AÑADIR PENDIENTE",
permite añadir un nuevo pendiente a la lista de pendientes.

El programa se realizó con el proposito de que sea util y facil de manejar, 
es por esto que se minimizó el margen de error del usuario, lo cual se logró
utilizando widgets como combo box y calendar, para que así el usuario no
tuviera que ingresar "a mano" las fechas y no hubieran problemas de formato.