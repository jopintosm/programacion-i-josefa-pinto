# Renata Arcos - Josefa Pinto San Martin / 29.11.2021
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from editar import ventana_editar
from eliminar import ventana_eliminar
from agregar import ventana_agregar
from ver import ventana_ver

class ventana_principal():
    
    def __init__(self):
        """Generar y mostrar ventana maximizada, además
        se termina el programa al cerrar la ventana."""
        builder = Gtk.Builder()
        builder.add_from_file("proyecto1.ui")
        self.ventana = builder.get_object("principal")
        self.ventana.set_title("PENDIENTES")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(800, 200)
        self.ventana.show_all()

        # mensaje principal
        label_principal = builder.get_object("label_principal")
        label_principal.set_label("POR FAVOR ELIJA UNA OPCION PARA CONTINUAR")

        # boton para mostrar todos los pendientes
        ver = builder.get_object("ver")
        ver.connect("clicked", self.ver_pendientes)
        ver.set_label("VER PENDIENTES")

        # boton para editar un pendiente en especifico
        editar = builder.get_object("editar")
        editar.connect("clicked", self.editar_pendiente)
        editar.set_label("EDITAR PENDIENTE")

        # boton para eliminar un pendiente en especifico
        eliminar = builder.get_object("eliminar")
        eliminar.connect("clicked", self.eliminar_pendiente)
        eliminar.set_label("ELIMINAR PENDIENTE")
        
        # boton para agregar un pendiente
        agregar = builder.get_object("agregar")
        agregar.connect("clicked", self.agregar_pendiente)
        agregar.set_label("AGREGAR PENDIENTE")

    def ver_pendientes(self, click=None):
        ventana_ver()


    def editar_pendiente(self, click=None):
        ventana_editar()


    def eliminar_pendiente(self, click=None):
        ventana_eliminar()
    
    
    def agregar_pendiente(self, click=None):
        ventana_agregar()
    



if __name__ == "__main__":
    ventana_principal()
    Gtk.main()