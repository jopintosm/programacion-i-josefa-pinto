# Renata Arcos - Josefa Pinto San Martin / 17.12.2021
import pandas as pd
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from about import ventana_about
from editar import ventana_editar
from dialogos import select_fila


class dialogo_chooser():
    
    def __init__(self):
    
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.chooser = builder.get_object("fc")
        self.chooser.set_title("ELEGIR ARCHIVOS")
        self.chooser.set_default_size(1200,900)
        
        self.chooser.set_action(Gtk.FileChooserAction.OPEN)
        self.chooser.show_all()

        # crear filtro de archivos
        filter_csv = Gtk.FileFilter()
        filter_csv.add_pattern("*.csv")
        filter_csv.set_name("Archivos CSV")
        self.chooser.add_filter(filter_csv)

        # boton OK
        ok_btn = builder.get_object("chooser_ok")
        ok_btn.connect("clicked", self.get_files)
        ok_btn.set_label("OK")

    
    def get_files(self, click=None):

        # seleccionar todos los archivos validos de la carpeta
        self.chooser.set_select_multiple(True)
        self.chooser.select_all()

        # obtener rutas de los archivos
        files = self.chooser.get_filenames()
        folder = self.chooser.get_current_folder()

        # hacer lista formateada para agregar posteriormente al combobox
        file_list = []
        for item in files:
            aux = item.replace(folder, "")[1:]
            aux = aux.replace(".csv", "")
            file_list.append(aux)

        self.chooser.destroy()
        ventana_principal(files, file_list, folder)
        return file_list, folder

class ventana_principal():
    
    def __init__(self, files, file_list, folder):
    
        builder = Gtk.Builder()
        builder.add_from_file("proyectofinal.ui")
        self.ventana = builder.get_object("principal")
        self.ventana.set_title("VENTANA PRINCIPAL")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(1200,900)
        self.ventana.show_all()
        self.files = files

        # boton about
        about_btn = builder.get_object("about")
        about_btn.connect("clicked", self.about)
        about_btn.set_label("ACERCA DE")

        # boton editar
        editar_btn = builder.get_object("editar")
        editar_btn.connect("clicked", self.editar)
        editar_btn.set_label("EDITAR")

        # boton resumen
        resumen_btn = builder.get_object("resumen")
        resumen_btn.connect("clicked", self.resumen)
        resumen_btn.set_label("RESUMEN")

        # review de la fila seleccionada
        self.label_review = builder.get_object("label")
        self.label_review.set_text("REVIEW")

        # realizar combo box
        self.combo = builder.get_object("combobox")
        self.lista = Gtk.ListStore(str)
        cell = Gtk.CellRendererText()
        self.combo.pack_start(cell, True)
        self.combo.add_attribute(cell, "text", 0)
        self.combo.connect("changed", self.cargar_archivos)
        
        # Se añaden files a lista modelo para combobox
        for file in file_list:
            self.lista.append([file])

        # Se carga modelo en combo box
        self.combo.set_model(self.lista)

        # Tree
        self.tree = builder.get_object("tree")
        
    def about(self, btn=None):
        ventana_about()
        

    def editar(self, btn=None):
        try:
            ventana_editar(self.review, self.ID, self.datos,self.path,self.nombre)
        except AttributeError:
            # mensaje de advertencia que debe seleccionar una fila
            select_fila()
            

    def resumen(self, btn=None):
        #ventana_resumen(self.datos)
        pass

        
    def cargar_archivos(self, btn=None):
        try:
            # Se obtiene iterador y modelo
            iterator = self.combo.get_active_iter()
            modelo = self.combo.get_model()
            # De lo anterior se obtiene el elemento
            # seleccionado del combobox
            self.año = modelo[iterator][0]
            
            # dependiendo del año seleccionado,
            # se elige una ruta
            if self.año == "2008":
                self.path = self.files[0]
            elif self.año == "2009":
                self.path = self.files[1]
            elif self.año == "2010":
                self.path = self.files[2]
            elif self.año == "2011":
                self.path = self.files[3]
            elif self.año == "2012":
                self.path = self.files[4]
            elif self.año == "2013":
                self.path = self.files[5]
            elif self.año == "2014":
                self.path = self.files[6]
            elif self.año == "2015":
                self.path = self.files[7]
            elif self.año == "2016":
                self.path = self.files[8]
            elif self.año == "2017":
                self.path = self.files[9]
            
            # cargar datos con la ruta del archivo
            self.datos = pd.read_csv(self.path)
            
            # eliminar review de la vista
            data = self.datos.drop(['review'], axis=1)
            data = data.drop("Unnamed: 0", axis=1)
            
            # generar treeview con los datos
            if self.tree.get_columns():
                for column in self.tree.get_columns():
                    self.tree.remove_column(column)

            largo_columnas = len(data.columns)
            modelo = Gtk.ListStore(*(largo_columnas * [str]))
            self.tree.set_model(model=modelo)
            cell = Gtk.CellRendererText()


            for item in range(len(data.columns)):
                column = Gtk.TreeViewColumn(data.columns[item],
                                            cell,
                                            text=item)
                self.tree.append_column(column)
                column.set_sort_column_id(item)

            for item in data.values:
                line = [str(x) for x in item]
                modelo.append(line)

            # presionar una fila y mostrar su review
            self.tree.connect("row-activated", self.seleccion)

        except AttributeError:
            # eligió mal la carpeta, el programa se termina
            self.ventana.destroy()

    def seleccion(self, btn=None, path=None, column=None):
        # obtener datos de la fila seleccionada en el treeview
        modelo, iterador = self.tree.get_selection().get_selected()
        self.ID = int(modelo.get_value(iterador,0))
        self.review = self.datos.loc[self.datos["uniqueID"]==self.ID,"review"].iloc[0]
        self.nombre = self.datos.loc[self.datos["uniqueID"]==self.ID,"drugName"].iloc[0]
        # setear el label con el texto del review de la linea seleccionada
        self.label_review.set_text(self.review)
        

if __name__ == "__main__":
    dialogo_chooser()
    Gtk.main()