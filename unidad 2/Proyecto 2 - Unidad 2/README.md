#Proyecto 2 - Unidad 2 / Renata Arcos - Josefa Pinto

El programa realizado es bastante util para lograr visualizar
datos de manera ordenada, tambien para poder editar las lineas de estos.
Este fue realizado de manera que sea facil e intuitivo de utilizar
para que de esta manera el usuario logre comprender su funcionamiento y
utilizarlos sin dificultad.

El programa comineza con un filechooser, donde se debe elegir una carpeta
que contenga archivos compatibles con el programa, luego ingresa a una ventana
principal donde se debe seleccionar un año para poder visualizar los datos
del archivo correspondiente al año elegido, además para mejorar la visualizacion
de los datos, al hacer doble click en una fila en el lado derecho se puede
observar el review del medicamento. Tambien se debe seleccionar una fila para
poder editarla, ya que al seleccionarla, se debe presionar el boton "EDITAR" para
que se habra otra ventana, donde todos los campos deben ser editados para lograr
una correcta edicion. Finalmente, en la ventana principal existe un boton llamado
"ACERCA DE", el cual entrega informacion basica sobre el programa y sus autores.